<?php
/**
 * @file
 * Page callbacks for access_code module.
 */

/**
 * Form callback for access code login form.
 */
function access_code_login($form, &$form_state) {
  global $user;

  if ($user->uid) {
    drupal_goto('user/' . $user->uid);
  }

  $form['access_code'] = array('#type' => 'textfield',
    '#title' => t('Access code'),
    '#description' => t('Enter your access code to log in.'),
    '#size' => 10,
    '#maxlength' => 20,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Log in'));

  return $form;
}

/**
 * Validation function for the access code login form.
 */
function access_code_login_validate($form, &$form_state) {
  $access = db_select('access_code')
    ->fields('access_code')
    ->condition('code', $form_state['values']['access_code'])
    ->execute()
    ->fetchAssoc();

  if (!empty($access) && $access['expiration'] > REQUEST_TIME) {
    $user_data = db_select('users')
      ->fields('users', array('uid'))
      ->condition('uid', $access['uid'])
      ->condition('status', 1)
      ->execute()
      ->fetchAssoc();
  }

  if (empty($user_data['uid'])) {
    $form_state['#uid'] = 0;
    form_set_error('access_code', t('Invalid access code.'));
  }
  else {
    $form_state['#uid'] = $user_data['uid'];
  }

}

/**
 * Submit function for the access code login form.
 */
function access_code_login_submit($form, &$form_state) {
  $form_state['uid'] = $form_state['#uid'];
  user_login_submit(array(), $form_state);
}

